import React, { useEffect } from "react";

import MovieListing from "../movieListing/MovieListing";

import { useDispatch } from "react-redux";
import {
  fetchAsyncMovies,
  fetchAsyncShows,
} from "../../features/movies/MovieSlice";

function Home() {
  const dispatch = useDispatch();

  const movieText = "Harry";
  const showText = "series";
  useEffect(() => {
    dispatch(fetchAsyncMovies(movieText));
    dispatch(fetchAsyncShows(showText));
  }, [dispatch]);

  return (
    <div>
      <div className="banner-img"></div>

      <MovieListing />
    </div>
  );
}

export default Home;
