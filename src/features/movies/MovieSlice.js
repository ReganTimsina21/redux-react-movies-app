import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import MovieApi from "../../common/apis/MovieApi";
import { APIKey } from "../../common/apis/MovieApiKey";

// fetchAsyncMovies
export const fetchAsyncMovies = createAsyncThunk(
  "movies/fetchAsyncMovies",
  async (term) => {
    try {
      const response = await MovieApi.get(
        `?apikey=${APIKey}&s=${term}&type=movie`
      );

      return response.data;
      //
    } catch (err) {
      console.log(`Err: `, err);
    }
  }
);

// fetchAsyncShows
export const fetchAsyncShows = createAsyncThunk(
  "movies/fetchAsyncShows",
  async (term) => {
    try {
      const response = await MovieApi.get(
        `?apikey=${APIKey}&s=${term}&type=series`
      );

      return response.data;
      //
    } catch (err) {
      console.log(`Err: `, err);
    }
  }
);

// fetchAsyncMovieOrShowDetail
export const fetchAsyncMovieOrShowDetail = createAsyncThunk(
  "movies/fetchAsyncMovieOrShowDetail",
  async (id) => {
    try {
      const response = await MovieApi.get(
        `?apikey=${APIKey}&i=${id}&Plot=full`
      );

      return response.data;
      //
    } catch (err) {
      console.log(`Err: `, err);
    }
  }
);

// initial state
const initialState = {
  movies: {},
  shows: {},
  selectMovieOrShow: {},
};

// movieSlice
const movieSlice = createSlice({
  name: "movies",
  initialState,
  reducers: {
    removeSelectedMovieorShow: (state, { payload }) => {
      state.selectMovieOrShow = {};
    },
  },
  extraReducers: {
    // fetchAsyncMovies
    [fetchAsyncMovies.pending]: (state, { payload }) => {
      console.log(`pending`);
    },
    [fetchAsyncMovies.fulfilled]: (state, { payload }) => {
      console.log("fetched successfully");
      return { ...state, movies: payload };
    },
    [fetchAsyncMovies.rejected]: (state, { payload }) => {
      console.log("rejected");
    },

    // fetchAsyncShows
    [fetchAsyncShows.fulfilled]: (state, { payload }) => {
      console.log(" Shows fetched successfully");
      return { ...state, shows: payload };
    },

    // fetchAsyncMovieOrShowDetail
    [fetchAsyncMovieOrShowDetail.fulfilled]: (state, { payload }) => {
      console.log(" fetchAsyncMovieOrShowDetail fetched successfully");
      return { ...state, selectMovieOrShow: payload };
    },
  },
});

export const { removeSelectedMovieorShow } = movieSlice.actions;

export const getAllMovies = (state) => state.movies.movies;
export const getAllShows = (state) => state.movies.shows;
export const getSelectMovieOrShow = (state) => state.movies.selectMovieOrShow;

export default movieSlice.reducer;
